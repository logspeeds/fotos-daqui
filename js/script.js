/*=============================================================================================================
..............................................GEOLOCATION URL...................................................
===============================================================================================================*/

let latitude = "";
let longitude = "";
let apiKey = '20a26b977512202d6884b503a307e60b';
let theme = 'corona';
let url = 'https://shrouded-mountain-15003.herokuapp.com/';

function geolocation() {
    if ('geolocation' in navigator) {
        let watcher = navigator.geolocation.getCurrentPosition(function (position) {
            latitude = position.coords.latitude;
            longitude = position.coords.longitude;
            url += 'https://flickr.com/services/rest/?api_key=' + apiKey +
                '&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=5&lat=' + latitude +
                '&lon=' + longitude +
                '&text=' + theme;
            console.log(position);
            console.log(url);
        },

            function (error) {
                latitude = -25.429767;
                longitude = -49.271968;
                url += 'https://flickr.com/services/rest/?api_key=' + apiKey +
                    '&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=5&lat=' + latitude +
                    '&lon=' + longitude +
                    '&text=' + theme;
                console.log(error)
            },

            {
                enableHighAccuracy: true,
                maximumAge: 12000000,
                timeout: 12000000
            })
    }
    else {
        latitude = -25.429767;
        longitude = -49.271968;
        url += 'https://flickr.com/services/rest/?api_key=' + apiKey +
            '&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=5&lat=' + latitude +
            '&lon=' + longitude +
            '&text=' + theme;
        alert('Não é possível obter a localização do usuário');
    }
}

geolocation()