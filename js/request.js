/*=============================================================================================================
.................................................FETCH..........................................................
===============================================================================================================*/
let flickrObj = {};

function fetchFlickr(url) {
    console.log(url)
    fetch(url)
        .then(function (request) {
            return request.json();
        })

        .then(function (data) {
            console.log(data);
            flickrObj = data;
        })
}

function constructImageURL(flickrObj) {
    let img = "https://farm" + flickrObj.farm +
        ".staticflickr.com/" + flickrObj.server +
        "/" + flickrObj.id + "_" + flickrObj.secret + ".jpg";
    console.log(img);
    const image = document.createElement('img');
    image.width = 380;
    image.height = 380;
    image.src = img;
    document.getElementById("photos").appendChild(image);
    return image;

}